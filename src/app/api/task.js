"use strict";

angular.module("app").factory("Task", function(Restangular) {
  /**
   * Retrieve a list of all tasks
   * @param [params] optional query parameters for pagination
   * @returns {*|{method, params, headers}}
   */
  function getAll(params) {
    params = params || {};
    return Restangular.one("tasks").get(params);
  }

  /**
   * Create a new task instance from an object
   * @param newTask object that contains the company data
   * @param [params] optional query params
   * @returns {*}
   */
  function create(newTask, params) {
    params = params || {};

    return Restangular.all("tasks").post(newTask, params);
  }

  return {
    all: getAll,
    create: create
  };
});
