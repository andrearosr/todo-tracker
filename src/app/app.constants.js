(function() {
  "use strict";

  angular
    .module("app")
    .constant("API", "https://rokk3r-todo.herokuapp.com/api/v1/");
})();
