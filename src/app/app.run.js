(function() {
  "use strict";

  angular.module("app").run(runBlock);

  /** @ngInject */
  function runBlock($log, $rootScope, Restangular) {
    const API_URL = "https://rokk3r-todo.herokuapp.com/api/v1/";

    Restangular.setBaseUrl(API_URL);
    Restangular.setFullResponse(true);
    Restangular.setRestangularFields({
      id: "_id"
    });

    $log.debug("App run block end");
  }
})();
