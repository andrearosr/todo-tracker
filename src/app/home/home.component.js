(function() {
  "use strict";

  angular.module("app").component("home", {
    controller: HomeController,
    controllerAs: "vm",
    templateUrl: "app/home/home.view.html"
  });

  /** @ngInject */
  function HomeController($log, Task) {
    var vm = this;
    vm.showOverdue = false;
    vm.tasks = [];

    vm.isShowOverdue = function(state) {
      return vm.showOverdue === state;
    };

    vm.toggle = function(newState) {
      vm.showOverdue = newState;
    };

    Task.all().then(function(response) {
      var docs = response.data.docs;

      vm.tasks = docs;
    });
  }
})();
