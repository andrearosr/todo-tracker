# README #

### What is this repository for? ###

* Frontend side of Todo-Tracker app

### How do I get set up? ###

Clone repo and run
`npm install`
then
`bower install`

To run in development mode:
`gulp serve`

#### Using app hosted in:
https://rokk3r-todo.herokuapp.com/api/v1/

#### API Swagger:
https://rokk3r-todo.herokuapp.com/api/v1/docs/#/
